package dev.ecbn.tiketdamri.module

import dev.ecbn.tiketdamri.BuildConfig
import dev.ecbn.tiketdamri.data.ServiceInterface
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val retrofitModule = module {
    single {
        okHttp()
    }
    single {
        retrofit()
    }
    single {
        get<Retrofit>().create(ServiceInterface::class.java)
    }
}

private fun okHttp() = OkHttpClient.Builder()
    .addInterceptor {
        val url = it.request()
            .url
            .newBuilder()
            .build()
        val req = it.request()
            .newBuilder()
            .url(url)
            .build()
        it.proceed(req)
    }
    .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC))
    .connectTimeout(60, TimeUnit.SECONDS)
    .build()

private fun retrofit() = Retrofit.Builder()
    .client(okHttp())
    .addConverterFactory(GsonConverterFactory.create())
    .baseUrl(BuildConfig.BASE_URL)
    .build()
