package dev.ecbn.tiketdamri.data

import dev.ecbn.tiketdamri.model.OriginResponse
import dev.ecbn.tiketdamri.model.TokenResponse
import retrofit2.http.*

/**
 * Tiket Damri Created by ecbn on 05/08/20.
 */
interface ServiceInterface {

    @FormUrlEncoded
    @POST(value = "authtimeout/token")
    suspend fun getToken(
        @Field(value = "username") pageNumber: String = "test",
        @Field(value = "password") withGenre: String = "damri123"
    ): TokenResponse

    @POST("apps/damriapps/getOrigin")
    suspend fun getOrigin(
        @Header("Authorization") token: String
    ): OriginResponse
}